# Face Generation Datasets

### Task Description

Face generation is the task of generating (or interpolating) new faces from an existing dataset.

The state-of-the-art results for this task are located in the Image Generation parent.



### Data Description

Face images mosts are aligned



### Dataset List

* CelebA-HQ
* FFHQ
* VGGFace
* VGGFace2