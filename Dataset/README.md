# Dataset

### Purpose
vision task들에서 사용되는 Dataset들을 공유하고 다른 맴버들의 재사용을 돕는다.



### How To Use

* Subdirectory 하나가 Task 한 개를 대표합니다.
* Subdirectory안에 Dataset별로 폴더를 만들고 데이터를 저장합니다.
* 각 Dataset 폴더마다 `README.md`를 작성합니다. `README.md` 에는 그 Dataset을 사용하기 위해 필요한 정보들을 적습니다.



### Subdirectories
Task를 알 수 있는 폴더명으로 하위 디렉토리를 만들고, 그 디렉토리에 Dataset별로 폴더를 생성한다.
Task명은 `papers with code` (https://paperswithcode.com/)를 참고하여 작성함
(알파벳순 정렬)

* Avatar
* Face_Generation
* Face_Recognition
* Fashion
* Video_Frame_Interpolation
* JPEG_Artifact_Removal

