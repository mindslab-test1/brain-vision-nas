# MS1MV2  (85K ids/5.8M images)

### Description

semi-automatic refined version of the MS-Celeb-1M dataset in Arcface paper

image size: 112x112 

sperated folder for each identity



### Downloaded From

* https://github.com/deepinsight/insightface/wiki/Dataset-Zoo



### References

* Yandong Guo, Lei Zhang, Yuxiao Hu, Xiaodong He, Jianfeng Gao. Ms-celeb-1m: A dataset and benchmark for large-scale face recognition. ECCV, 2016.
*  Jiankang Deng, Jia Guo, Stefanos Zafeiriou. Arcface: Additive angular margin loss for deep face recognition, arXiv:1801.07698, 2018.



### Cited by

* Jiankang Deng, Jia Guo, Niannan Xue, Stefanos Zafeiriou. ArcFace: Additive Angular Margin Loss for Deep Face Recognition. arXiv:1801.07698, 2018