# Face Recognition Datasets

### Task Description

Facial recognition is the task of making a positive identification of a face in a photo or video image against a pre-existing database of faces. It begins with detection - distinguishing human faces from other objects in the image - and then works on identification of those detected faces.

The state of the art tables for this task are contained mainly in the consistent parts of the task : the face verification and face identification tasks.



### Data Description

Face images, identity label



### Dataset List

* Asian-Celeb
* CASIA-Webface
* DeepGlint
* LFW
* MS1MV2
* MS1M-IBUG
* UMDFace
* VGG2