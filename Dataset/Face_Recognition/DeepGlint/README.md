# DeepGlint (181K ids/6.75M images)

### Description

image size: 112x112 

sperated folder for each identity



### Downloaded From

* https://github.com/deepinsight/insightface/wiki/Dataset-Zoo



### References

* http://trillionpairs.deepglint.com/overview



### Cited by

* Jiankang Deng, Jia Guo, Niannan Xue, Stefanos Zafeiriou. ArcFace: Additive Angular Margin Loss for Deep Face Recognition. arXiv:1801.07698, 2018