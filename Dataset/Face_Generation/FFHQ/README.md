# Flickr-Faces-HQ Dataset (FFHQ)

### Description

The dataset consists of 70,000 high-quality PNG images at 1024×1024 resolution and contains considerable variation in terms of age, ethnicity and image background. It also has good coverage of accessories such as eyeglasses, sunglasses, hats, etc. The images were crawled from [Flickr](https://www.flickr.com/), thus inheriting all the biases of that website, and automatically aligned and cropped using [dlib](http://dlib.net/). Only images under permissive licenses were collected. Various automatic filters were used to prune the set, and finally [Amazon Mechanical Turk](https://www.mturk.com/) was used to remove the occasional statues, paintings, or photos of photos.



### Downloaded From

* https://github.com/NVlabs/ffhq-dataset



### References

* Karras, Tero, Samuli Laine, and Timo Aila. "A style-based generator architecture for generative adversarial networks." *Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition*. 2019.



### Cited by

* Li, Lingzhi, et al. "FaceShifter: Towards High Fidelity And Occlusion Aware Face Swapping." *arXiv preprint arXiv:1912.13457* (2019).