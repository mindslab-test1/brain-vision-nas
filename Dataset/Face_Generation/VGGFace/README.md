# VGGFace

### Description

image size: ???x???

Face images

Identities: 2,622 

Number of images: 2.6M form original paper, 1420224 images was downloaded.
(skimage.io로 read가 가능하고 256 * 256이상의 이미지수: 1117466)

Number of aligned images: ???



### Downloaded From

* https://github.com/ndaidong/vgg-faces-utils
* github 코드를 그대로 사용하면 너무 느려서 multiprocessing을 활용하여 24시간 정도 소요하여 다운로드 받았다.



### References

* https://www.robots.ox.ac.uk/~vgg/data/vgg_face/
* Parkhi, Omkar M., Andrea Vedaldi, and Andrew Zisserman. "Deep face recognition." (2015).



### Cited by

* Li, Lingzhi, et al. "FaceShifter: Towards High Fidelity And Occlusion Aware Face Swapping." *arXiv preprint arXiv:1912.13457* (2019).