# Fashion Dataset

### Task Description

Fashion Dataset은 garment, clothes 등 의류에 대한 데이터셋으로, 착장 데이터, 독립적인 의류 사진 등을 모두 포함한다.
활용할 수 있는 Task로는 아래와 같다.
* VTON(Virtual Try-On)
* Keypoint Estimation
* Clothes Retrieval(의류 추천) 


### Data Description



### Dataset List

* DeepFashion2