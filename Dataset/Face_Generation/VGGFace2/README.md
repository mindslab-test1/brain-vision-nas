# Asian-Celeb (94K ids/2.8M images)

### Description

VGGFace2 is a large-scale face recognition dataset. Images are downloaded from Google Image Search and have large variations in pose, age, illumination, ethnicity and profession.

9K identities, 3.3M faces



### Downloaded From

* https://github.com/deepinsight/insightface/wiki/Dataset-Zoo



### References

* http://www.robots.ox.ac.uk/~vgg/data/vgg_face2/
* Cao, Qiong, et al. "Vggface2: A dataset for recognising faces across pose and age." *2018 13th IEEE International Conference on Automatic Face & Gesture Recognition (FG 2018)*. IEEE, 2018.



### Cited by

* Jiankang Deng, Jia Guo, Niannan Xue, Stefanos Zafeiriou. ArcFace: Additive Angular Margin Loss for Deep Face Recognition. arXiv:1801.07698, 2018