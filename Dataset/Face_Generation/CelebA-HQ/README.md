# CelebA-HQ

### Description

10K aiigned faces

Image size: 1024x1024

File format: JPEG



### Downloaded From

* https://drive.google.com/open?id=11Vz0fqHS2rXDb5pprgTjpD7S2BAJhi1P
* 공개된 link를 통해 download받고 전처리하는 방식들은 문제가 많아 zip파일 형식으로 비공식적으로 올려진 파일을 다운 받았다.



### References

* Karras, Tero, et al. "Progressive growing of gans for improved quality, stability, and variation." *arXiv preprint arXiv:1710.10196* (2017).



### Cited by

* Li, Lingzhi, et al. "FaceShifter: Towards High Fidelity And Occlusion Aware Face Swapping." *arXiv preprint arXiv:1912.13457* (2019).