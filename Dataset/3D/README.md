# 3D Dataset

### Task Description

이름이 `3D`인 데에 대해 대단히 죄송하다. 아직 3D domain에 대한 dataset들 간 convention이 굳혀 있지 않고,
dataset이 어떻게 될 지 몰라서 `3D`라는 이름을 썼다.


### Data Description

* 2D in-the-wild images with partial depth map


### Dataset List

* OASIS