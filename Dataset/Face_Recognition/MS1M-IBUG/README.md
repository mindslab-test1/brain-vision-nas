# MS1M-IBUG (85K ids/3.8M images)

### Description

image size: 112x112 

sperated folder for each identity



### Downloaded From

* https://github.com/deepinsight/insightface/wiki/Dataset-Zoo



### References

* Yandong Guo, Lei Zhang, Yuxiao Hu, Xiaodong He, Jianfeng Gao. Ms-celeb-1m: A dataset and benchmark for large-scale face recognition. ECCV, 2016.
* Jiankang Deng, Yuxiang Zhou, Stefanos Zafeiriou. Marginal loss for deep face recognition, CVPRW, 2017.



### Cited by

* Jiankang Deng, Jia Guo, Niannan Xue, Stefanos Zafeiriou. ArcFace: Additive Angular Margin Loss for Deep Face Recognition. arXiv:1801.07698, 2018