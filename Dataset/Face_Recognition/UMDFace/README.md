# UMDFace (8K ids/0.37M images)

### Description

The dataset contains 367,888 face annotations for 8,277 subjects.

image size: 112x112 

sperated folder for each identity



### Downloaded From

* https://github.com/deepinsight/insightface/wiki/Dataset-Zoo



### References

* Bansal Ankan, Nanduri Anirudh, Castillo Carlos D, Ranjan Rajeev, Chellappa, Rama. UMDFaces: An Annotated Face Dataset for Training Deep Networks, arXiv:1611.01484v2, 2016.
* https://www.umdfaces.io/



### Cited by

* Jiankang Deng, Jia Guo, Niannan Xue, Stefanos Zafeiriou. ArcFace: Additive Angular Margin Loss for Deep Face Recognition. arXiv:1801.07698, 2018